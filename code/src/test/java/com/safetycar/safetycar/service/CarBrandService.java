package com.safetycar.safetycar.service;

import com.safetycar.safetycar.models.CarBrand;
import com.safetycar.safetycar.repositories.interfaces.CarBrandRepository;
import com.safetycar.safetycar.services.CarBrandServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class CarBrandService {

    @InjectMocks
    CarBrandServiceImpl carBrandService;

    @Mock
    CarBrandRepository mockCarBrandRepository;

    @Test
    public void getById_ShouldReturn_WhenValidId() {
        //Arrange
        CarBrand originalBrand = new CarBrand();
        Mockito.when(mockCarBrandRepository.getBrandById(Mockito.anyInt()))
                .thenReturn(originalBrand);

        //Act
        CarBrand returnedBrand = carBrandService.getBrandById(Mockito.anyInt());

        //Assert
        Assertions.assertEquals(returnedBrand, originalBrand);
    }

    @Test
    public void getAll_ShouldReturn_WhenValidRequest() {
        //Arrange
        List<CarBrand> originalBrands = new ArrayList<>();
        Mockito.when(mockCarBrandRepository.getAllBrands()).thenReturn(originalBrands);

        //Act
        List<CarBrand> returnedBrands = carBrandService.getAllBrands();

        //Assert
        Assertions.assertEquals(returnedBrands, originalBrands);
    }
}
