package com.safetycar.safetycar.service;

import com.safetycar.safetycar.models.CarBrand;
import com.safetycar.safetycar.models.CarModel;
import com.safetycar.safetycar.repositories.interfaces.CarModelRepository;
import com.safetycar.safetycar.services.CarModelServiceImpl;
import com.safetycar.safetycar.services.interfaces.CarBrandService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class CarModelService {

    @InjectMocks
    CarModelServiceImpl carModelService;

    @Mock
    CarModelRepository mockCarModelRepository;

    @Mock
    CarBrandService mockCarBrandService;

    @Test
    public void getById_ShouldReturn_WhenValidId() {
        //Arrange
        CarModel originalModel = new CarModel();
        Mockito.when(mockCarModelRepository.getModelById(Mockito.anyInt()))
                .thenReturn(originalModel);

        //Act
        CarModel returnedModel = carModelService.getModelById(Mockito.anyInt());

        //Assert
        Assertions.assertEquals(returnedModel, originalModel);
    }

    @Test
    public void getAll_ShouldReturn_WhenValidRequest() {
        //Arrange
        List<CarModel> originalModels = new ArrayList<>();
        Mockito.when(mockCarModelRepository.getAllModels()).thenReturn(originalModels);

        //Act
        List<CarModel> returnedModels = carModelService.getAllModels();

        //Assert
        Assertions.assertEquals(returnedModels, originalModels);
    }

    @Test
    public void getAllByBrand_ShouldReturn_WhenValidRequest() {
        //Arrange
        List<CarModel> originalModels = new ArrayList<>();
        CarBrand brand = new CarBrand();
        Mockito.when(mockCarModelRepository.getModelsByBrand(brand.getId())).thenReturn(originalModels);

        //Act
        List<CarModel> returnedModels = carModelService.getModelsByBrand(Mockito.anyInt());

        //Assert
        Assertions.assertEquals(returnedModels, originalModels);
    }
}
