package com.safetycar.safetycar.service;

import com.safetycar.safetycar.models.PolicyStatus;
import com.safetycar.safetycar.repositories.interfaces.PolicyStatusRepository;
import com.safetycar.safetycar.services.PolicyStatusServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class PolicyStatusService {

    @InjectMocks
    PolicyStatusServiceImpl policyStatusService;

    @Mock
    PolicyStatusRepository mockPolicyStatusRepository;

    @Test
    public void getById_ShouldReturn_WhenValidId() {
        //Arrange
        PolicyStatus originalStatus = new PolicyStatus();
        Mockito.when(mockPolicyStatusRepository.getPolicyStatusById(Mockito.anyInt()))
                .thenReturn(originalStatus);

        //Act
        PolicyStatus returnedStatus = policyStatusService.getPolicyStatusById(Mockito.anyInt());

        //Assert
        Assertions.assertEquals(returnedStatus, originalStatus);
    }

    @Test
    public void getByName_ShouldReturn_WhenValidName() {
        //Arrange
        PolicyStatus originalStatus = new PolicyStatus();
        Mockito.when(mockPolicyStatusRepository.getPolicyStatusByName(Mockito.anyString()))
                .thenReturn(originalStatus);

        //Act
        PolicyStatus returnedStatus = policyStatusService.getPolicyStatusByName(Mockito.anyString());

        //Assert
        Assertions.assertEquals(returnedStatus, originalStatus);
    }

    @Test
    public void getAll_ShouldReturn_WhenValidRequest() {
        //Arrange
        List<PolicyStatus> originalStatuses = new ArrayList<>();
        Mockito.when(mockPolicyStatusRepository.getAllStatuses()).thenReturn(originalStatuses);

        //Act
        List<PolicyStatus> returnedStatuses  = policyStatusService.getAllStatuses();

        //Assert
        Assertions.assertEquals(returnedStatuses, originalStatuses);
    }
}
