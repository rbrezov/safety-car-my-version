package com.safetycar.safetycar.service;

import com.safetycar.safetycar.models.Offer;
import com.safetycar.safetycar.repositories.interfaces.OfferRepository;
import com.safetycar.safetycar.services.OfferServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class OfferService {

    @InjectMocks
    OfferServiceImpl offerService;

    @Mock
    OfferRepository mockOfferRepository;

    @Test
    public void getById_ShouldReturn_WhenValidId() {
        //Arrange
        Offer originalOffer = new Offer();
        Mockito.when(mockOfferRepository.getOfferById(Mockito.anyInt()))
                .thenReturn(originalOffer);

        //Act
        Offer returnedOffer = offerService.getOfferById(Mockito.anyInt());

        //Assert
        Assertions.assertEquals(returnedOffer, originalOffer);
    }

    @Test
    public void getAll_ShouldReturn_WhenValidRequest() {
        //Arrange
        List<Offer> originalOffers = new ArrayList<>();
        Mockito.when(mockOfferRepository.getAllOffers()).thenReturn(originalOffers);

        //Act
        List<Offer> returnedOffers = offerService.getAllOffers();

        //Assert
        Assertions.assertEquals(returnedOffers, originalOffers);
    }

    @Test
    public void getCarAge_ShouldReturn_WhenValidRequest() {
        //Arrange
        Offer testOffer = new Offer();
        Mockito.when(mockOfferRepository.getCarAge(testOffer)).thenReturn(Mockito.anyInt());

        //Act
        offerService.getCarAge(testOffer);

        //Assert
        Mockito.verify(mockOfferRepository, Mockito.times(1)).getCarAge(testOffer);
    }

    @Test
    public void calculateTotalPremium_ShouldReturn_WhenValidRequest() {
        //Arrange
        Offer testOffer = new Offer();
        Mockito.when(mockOfferRepository.calculateTotalPremium(testOffer)).thenReturn(Mockito.anyDouble());

        //Act
        offerService.calculateTotalPremium(testOffer);

        //Assert
        Mockito.verify(mockOfferRepository, Mockito.times(1)).calculateTotalPremium(testOffer);
    }

    @Test
    public void create_ShouldCreate_WhenAllParametersAreCorrect() {
        //Arrange
        Offer testOffer = new Offer();

        //Act
        offerService.createOffer(testOffer);

        //Assert
        Mockito.verify(mockOfferRepository, Mockito.times(1)).createOffer(testOffer);
    }

    @Test
    public void delete_ShouldDelete_WhenValidId(){
        //Arrange
        Offer testOffer = new Offer();

        //Act
        offerService.deleteOffer(testOffer);

        //Assert
        Mockito.verify(mockOfferRepository, Mockito.times(1)).deleteOffer(testOffer);

    }


}
