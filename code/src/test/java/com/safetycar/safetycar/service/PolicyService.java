package com.safetycar.safetycar.service;

import com.safetycar.safetycar.exceptions.UnauthorizedOperationException;
import com.safetycar.safetycar.models.*;
import com.safetycar.safetycar.repositories.interfaces.PolicyRepository;
import com.safetycar.safetycar.services.PolicyServiceImpl;
import com.safetycar.safetycar.services.interfaces.PolicyStatusService;
import org.apache.tomcat.jni.Local;
import org.hibernate.annotations.NaturalIdCache;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class PolicyService {

    @InjectMocks
    PolicyServiceImpl policyService;

    @Mock
    PolicyRepository mockPolicyRepository;

    @Mock
    PolicyStatusService policyStatusService;

    @Mock
    OfferService offerService;

    private UserDetails testUser;
    private CarBrand testCarBrand;
    private CarModel testCarModel;
    private Offer testOffer;
    private Policy testPolicy;

    @BeforeEach
    public void before(){
        testUser = new UserDetails();
        testUser.setEmailAddress("genadi@abv.bg");
        testUser.setAddress("Sofia");
        testUser.setPhoneNumber("+359887222222");
        testUser.setFirstName("Genadi");
        testUser.setLastName("Petrov");
        testUser.setId(1);
        testUser.setIsActive(true);

        testCarBrand = new CarBrand();
        testCarBrand.setName("Opel");
        testCarBrand.setId(1);

        testCarModel = new CarModel();
        testCarModel.setCarBrand(testCarBrand);
        testCarModel.setId(1);
        testCarModel.setModelYear(2000);
        testCarModel.setName("Astra");

        testOffer = new Offer();
        testOffer.setRequestNumber(100002);
        testOffer.setTotalPremium(1000);
        testOffer.setPreviousAccidents(true);
        testOffer.setDriverAge(33);
        testOffer.setCubicCapacity(2000);
        testOffer.setCarModel(testCarModel);
        testOffer.setId(1);

        testPolicy = new Policy();
        byte[] photo = new byte[43];
        testPolicy.setRegistrationCertificate(photo);
        testPolicy.setEndDate(LocalDate.now());
        testPolicy.setStartDate(LocalDate.now());
        testPolicy.setPolicyStatus(policyStatusService.getPolicyStatusById(1));
        testPolicy.setOffer(testOffer);
        testPolicy.setId(1);
        testPolicy.setSubmitDate(LocalDateTime.now());
        testPolicy.setFirstName("Ivan");
        testPolicy.setLastName("Ivanov");
        testPolicy.setPostalAddress("Varna");
        testPolicy.setEmailAddress("ivan@ivan.bg");
        testPolicy.setPhoneNumber("+359887888888");
        testPolicy.setOwner(testUser);
    }

    @Test
    public void getById_ShouldReturn_WhenValidId() {
        //Arrange
        Policy originalPolicy = new Policy();
        Mockito.when(mockPolicyRepository.getPolicyById(Mockito.anyInt()))
                .thenReturn(originalPolicy);

        //Act
        Policy returnedPolicy = policyService.getPolicyById(Mockito.anyInt());

        //Assert
        Assertions.assertEquals(returnedPolicy, originalPolicy);
    }

    @Test
    public void getAllByOwner_ShouldReturn_WhenValidId() {
        //Arrange
        List<Policy> policies = new ArrayList<>();
        Mockito.when(mockPolicyRepository.getAllByOwner(1))
                .thenReturn(policies);

        //Act
        List<Policy> returnedPolicies = policyService.getAllByOwner(1);

        //Assert
        Assertions.assertEquals(returnedPolicies, policies);
    }

    @Test
    public void getAll_ShouldReturn_WhenValidRequest() {
        //Arrange
        List<Policy> originalPolicies = new ArrayList<>();
        Mockito.when(mockPolicyRepository.getAllPolicies()).thenReturn(originalPolicies);

        //Act
        List<Policy> returnedPolicies = policyService.getAllPolicies();

        //Assert
        Assertions.assertEquals(returnedPolicies, originalPolicies);
    }

    @Test
    public void create_ShouldCreate_WhenAllParametersAreCorrect() {
        //Arrange
        Policy testPolicy = new Policy();
        UserDetails testUser = new UserDetails();
        PolicyStatus testStatus = policyStatusService.getPolicyStatusById(1);

        //Act
        policyService.createPolicy(testPolicy, testUser);

        //Assert
        Mockito.verify(mockPolicyRepository, Mockito.times(1)).createPolicy(testPolicy);
    }

    @Test
    public void update_ShouldUpdate_WhenRequestorIsValid(){
        //Arrange
        policyService.updatePolicy(testPolicy, testUser);

        //Assert
        Mockito.verify(mockPolicyRepository, Mockito.times(1)).updatePolicy(testPolicy);
    }

    @Test
    public void update_ShouldThrow_WhenRequestorIsNotAuthorized() {
        //Arrange
        UserDetails notOwner = new UserDetails();
        notOwner.setEmailAddress("ivan@ivan.bg");
        notOwner.setAddress("Varna");
        notOwner.setPhoneNumber("+359887888888");
        notOwner.setFirstName("Ivan");
        notOwner.setLastName("Ivanov");
        notOwner.setId(1);
        notOwner.setIsActive(true);

        //Act, Assert
        policyService.updatePolicy(testPolicy, testUser);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> policyService.updatePolicy(testPolicy, notOwner));

    }

    @Test
    public void delete_ShouldUpdate_WhenRequestorIsValid(){
         //Arrange
        policyService.deletePolicy(testPolicy, testUser);

        //Assert
        Mockito.verify(mockPolicyRepository, Mockito.times(1)).deletePolicy(testPolicy);
    }

    @Test
    public void delete_ShouldThrow_WhenRequestorIsNotAuthorized() {
        //Arrange
        UserDetails notOwner = new UserDetails();
        notOwner.setEmailAddress("ivan@ivan.bg");
        notOwner.setAddress("Varna");
        notOwner.setPhoneNumber("+359887888888");
        notOwner.setFirstName("Ivan");
        notOwner.setLastName("Ivanov");
        notOwner.setId(1);
        notOwner.setIsActive(true);

        //Act, Assert
        policyService.deletePolicy(testPolicy, testUser);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> policyService.deletePolicy(testPolicy, notOwner));

    }

    @Test
    public void cancel_ShouldThrow_WhenRequestorIsNotAuthorized() {
        //Arrange
        UserDetails notOwner = new UserDetails();
        notOwner.setEmailAddress("ivan@ivan.bg");
        notOwner.setAddress("Varna");
        notOwner.setPhoneNumber("+359887888888");
        notOwner.setFirstName("Ivan");
        notOwner.setLastName("Ivanov");
        notOwner.setId(1);
        notOwner.setIsActive(true);

        //Act, Assert
        policyService.cancelPolicy(testPolicy, testUser);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> policyService.cancelPolicy(testPolicy, notOwner));

    }
}
