var myObj =
    {
        init:function ()
        {
            this.loadBrands();
        },
        loadBrands:function ()
        {
            var xhr = new XMLHttpRequest();
            xhr.open('GET', 'http://localhost:8080/api/brands/all', true);
            xhr.onload = function ()
            {
                var brands = xhr.responseText;
                brands.forEach(function (value) {
                    var op = document.createElement('option');
                    op.innerText = value.name;
                    op.setAttribute('value', value.id);
                    document.getElementById('brand').appendChild(op);
                });


            }
            xhr.send();
        }
    }
    myObj.init();