$(document).ready(function (){
    loadBrands();

    $("select#brand-select").change(function (){
        console.log("this", $(this));
        var id = $(this).children("option:selected").val();
        console.log("id", id);
        clearModels();
        loadModels(++id);
    });

    $("select#model-select").change(function () {
        var title = $(this).children("option:selected").text();
        $('#models').append('<p>' + title + '</p>');
    });
});

function loadBrands() {
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/api/brands/all',
        success: function (data) {
            console.log("brands");
            console.log(data);
            $('#brand-select').append('<option selected disabled th:value="-1"> Select the brand of your car</option>')
            $('#model-select').append('<option selected disabled th:value="-1"> Select the model of your car</option>')
            $.each(data, function (key, value) {
                var html = createOptionHtml(key, value.name);
                //console.log("value", value)
                console.log($('#brand-select'));
                $('#brand-select').append(html);
            });
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function loadModels(brandId) {
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/api/models/search' + brandId,
        success: function (data) {
            console.log(data);
            $('#model-select').append('<option selected disabled th:value="-1">Select the model of your car</option>')
            $.each(data, function (key, value) {
                var html = createOptionHtml(key, value.name);
                // console.log(html);
                console.log($('#brand-select'));
                $('#model-select').append(html);
            });
        }
    });
}

function clearModels() {
    $('#model-select').empty();
}

function createOptionHtml(key, value) {
    return '<option value="' + key + '">' + value + '</option>';
}