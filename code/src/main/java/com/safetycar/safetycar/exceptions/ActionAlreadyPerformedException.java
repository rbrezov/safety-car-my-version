package com.safetycar.safetycar.exceptions;

public class ActionAlreadyPerformedException extends RuntimeException {

    public ActionAlreadyPerformedException(String message) {
        super(message);
    }
}
