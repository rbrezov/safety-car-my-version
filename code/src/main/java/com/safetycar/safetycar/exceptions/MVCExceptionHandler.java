package com.safetycar.safetycar.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.springframework.http.HttpStatus.*;

@ControllerAdvice("com.safetycar.safetycar.controllers.mvc")
public class MVCExceptionHandler{

    @ExceptionHandler(value = {DuplicateEntityException.class})
    public ModelAndView handleDuplicateEntityException(DuplicateEntityException ex) {
        return buildView(ex, CONFLICT);
    }

    @ExceptionHandler(value = {EntityNotFoundException.class})
    public ModelAndView handleEntityNotFoundException(EntityNotFoundException ex) {
        return buildView(ex, NOT_FOUND);
    }

    @ExceptionHandler(value = {UnauthorizedOperationException.class})
    public ModelAndView handleUnauthorizedOperationException(UnauthorizedOperationException ex) {
        return buildView(ex, UNAUTHORIZED);
    }

    @ExceptionHandler(value = {ActionAlreadyPerformedException.class})
    public ModelAndView handleActionAlreadyPerformedException(ActionAlreadyPerformedException ex) {
        return buildView(ex, CONFLICT);
    }

    private ModelAndView buildView(RuntimeException ex, HttpStatus status) {
        ModelAndView modelAndView = new ModelAndView();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm:ss");
        modelAndView.setViewName("error");
        modelAndView.addObject("message", ex.getMessage());
        modelAndView.addObject("statusCode", status.value());
        modelAndView.addObject("codeTitle", status.getReasonPhrase());
        modelAndView.addObject("timeStamp", dtf.format(LocalDateTime.now()));
        return modelAndView;
    }
}
