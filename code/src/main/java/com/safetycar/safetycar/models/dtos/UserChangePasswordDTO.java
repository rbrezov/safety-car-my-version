package com.safetycar.safetycar.models.dtos;

import javax.validation.constraints.Size;

public class UserChangePasswordDTO {

    @Size(min = 6, max = 63, message = "Password must be between {min} and {max} characters long.")
    private String oldPassword;

    @Size(min = 6, max = 63, message = "Password must be between {min} and {max} characters long.")
    private String newPassword;

    @Size(min = 6, max = 63, message = "Password confirmation must be between {min} and {max} characters long.")
    private String passwordConfirmation;

    public UserChangePasswordDTO() {
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}
