package com.safetycar.safetycar.models;

import javax.persistence.*;

@Entity
@IdClass(AuthorityId.class)
@Table(name = "authorities")
public class Authority {

    @Id
    @Column(name = "username")
    private String email;

    @Id
    @Column(name = "authority")
    private String authority;

    public Authority() {
    }

    public Authority(String email, String authority) {
        this.email = email;
        this.authority = authority;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}
