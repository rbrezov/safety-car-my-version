package com.safetycar.safetycar.models.dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class UserRegistrationDTO {

    @Email
    @Size(min = 8, max = 40, message = "Email address must be between {min} and {max} characters long.")
    private String emailAddress;

    @Size(min = 6, max = 63, message = "Password must be between {min} and {max} characters long.")
    private String password;

    @NotEmpty
    private String passwordConfirmation;

    @Size(min = 2, max = 30, message = "First name must be between {min} and {max} characters long.")
    private String firstName;

    @Size(min = 2, max = 30, message = "Last name must be between {min} and {max} characters long.")
    private String lastName;

    @Size(max = 100, message = "Address cannot be more than {max} characters long.")
    private String address;

    @Size(max = 20, message = "Phone number cannot be more than {max} characters long.")
    private String phoneNumber;

    public UserRegistrationDTO() {
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
