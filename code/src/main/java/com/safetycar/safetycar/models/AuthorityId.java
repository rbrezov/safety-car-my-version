package com.safetycar.safetycar.models;

import java.io.Serializable;
import java.util.Objects;

public class AuthorityId implements Serializable {

    private String email;
    private String authority;

    public AuthorityId() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuthorityId that = (AuthorityId) o;
        return Objects.equals(email, that.email) &&
                Objects.equals(authority, that.authority);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, authority);
    }
}
