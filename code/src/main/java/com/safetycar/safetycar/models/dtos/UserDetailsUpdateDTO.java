package com.safetycar.safetycar.models.dtos;

import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class UserDetailsUpdateDTO {

    private int id;

    @Size(min = 2, max = 30, message = "First name must be between {min} and {max} characters long.")
    private String firstName;

    @Size(min = 2, max = 30, message = "Last name must be between {min} and {max} characters long.")
    private String lastName;

    @Size(max = 100, message = "Address cannot be more than {max} characters long.")
    private String address;

    @Size(max = 20, message = "Phone number cannot be more than {max} characters long.")
    private String phoneNumber;

    public UserDetailsUpdateDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
