package com.safetycar.safetycar.models.dtos;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

public class PolicyDto {

    @NotNull(message = "*Please fill in the date.")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @Size(min=2, max=50, message = "*First name must be between {min} and {max} characters long.")
    private String firstName;

    @Size(min=2, max=50, message = "*Last name must be between {min} and {max} characters long.")
    private String lastName;

    @Size(min=13, max=13, message = "*Phone number must be {min} characters in format: +359XXXXXXXXX")
    private String phoneNumber;

    @Email(message = "*Email address should be valid.")
    private String emailAddress;

    @Size(min=10, max=200, message = "*Postal address must be between {min} and {max} characters")
    private String postalAddress;

    @NotNull(message = "*Please upload your registration certificate.")
    private MultipartFile registrationCertificate;

    public PolicyDto() {
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public MultipartFile getRegistrationCertificate() {
        return registrationCertificate;
    }

    public void setRegistrationCertificate(MultipartFile registrationCertificate) {
        this.registrationCertificate = registrationCertificate;
    }
}
