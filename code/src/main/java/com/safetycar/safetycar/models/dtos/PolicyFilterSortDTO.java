package com.safetycar.safetycar.models.dtos;

import java.time.LocalDate;

public class PolicyFilterSortDTO {

    private int statusId;
    private LocalDate startDate;

    public PolicyFilterSortDTO() {
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }
}
