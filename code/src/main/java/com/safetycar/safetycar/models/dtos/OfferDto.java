package com.safetycar.safetycar.models.dtos;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.time.LocalDate;

public class OfferDto {

    @PositiveOrZero(message = "*Cubic capacity cannot be a negative number.")
    private int cubicCapacity;

    @Positive(message = "*Please select a car brand.")
    private int carBrandId;

    @Positive(message = "*Please select a car model.")
    private int carModelId;

    @NotNull(message = "*Please fill in the date.")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate registrationDate;

    @Min(value = 18, message = "*The legally permitted minimum driving age in Bulgaria is {value}.")
    @Max(value = 100, message = "*Please input a valid age.")
    private int driverAge;

    private boolean previousAccidents;

    public OfferDto() {
    }

    public int getCubicCapacity() {
        return cubicCapacity;
    }

    public void setCubicCapacity(int cubicCapacity) {
        this.cubicCapacity = cubicCapacity;
    }

    public int getCarBrandId() {
        return carBrandId;
    }

    public void setCarBrandId(int carBrandId) {
        this.carBrandId = carBrandId;
    }

    public int getCarModelId() {
        return carModelId;
    }

    public void setCarModelId(int carModelId) {
        this.carModelId = carModelId;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    public int getDriverAge() {
        return driverAge;
    }

    public void setDriverAge(int driverAge) {
        this.driverAge = driverAge;
    }

    public boolean isPreviousAccidents() {
        return previousAccidents;
    }

    public void setPreviousAccidents(boolean previousAccidents) {
        this.previousAccidents = previousAccidents;
    }

}
