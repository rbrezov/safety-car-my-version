package com.safetycar.safetycar.controllers.rest;

import com.safetycar.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.safetycar.models.CarModel;
import com.safetycar.safetycar.services.interfaces.CarModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/models")
public class CarModelRestController {

    private final CarModelService carModelService;

    @Autowired
    public CarModelRestController(CarModelService carModelService) {
        this.carModelService = carModelService;
    }

    @GetMapping("/all")
    public List<CarModel> getAllModels() {
        return carModelService.getAllModels();
    }

    @GetMapping("/{id}")
    public CarModel getModelById(@PathVariable int id) {
        return carModelService.getModelById(id);
    }

    @GetMapping("/search")
    public List<CarModel> searchModels(@RequestParam(required = false) Integer brandId) {
        if (brandId != null && brandId > 0) {
            return carModelService.getModelsByBrand(brandId);
        } else {
            throw new EntityNotFoundException("There are no models matching this brand.");
        }
    }
}
