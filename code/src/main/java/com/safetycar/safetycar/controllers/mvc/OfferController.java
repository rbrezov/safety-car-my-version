package com.safetycar.safetycar.controllers.mvc;

import com.safetycar.safetycar.models.Offer;
import com.safetycar.safetycar.models.dtos.OfferDto;
import com.safetycar.safetycar.services.interfaces.CarBrandService;
import com.safetycar.safetycar.services.interfaces.CarModelService;
import com.safetycar.safetycar.services.interfaces.OfferService;
import com.safetycar.safetycar.utils.OfferMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@SessionAttributes("newOffer")
@RequestMapping("/offers")
public class OfferController {

    private final OfferService offerService;
    private final OfferMapper offerMapper;
    private final CarModelService carModelService;
    private final CarBrandService carBrandService;

    @Autowired
    public OfferController(OfferService offerService, OfferMapper offerMapper, CarModelService carModelService, CarBrandService carBrandService) {
        this.offerService = offerService;
        this.offerMapper = offerMapper;
        this.carModelService = carModelService;
        this.carBrandService = carBrandService;
    }

    @GetMapping("/new")
    public String getNewOfferPage(Model model) {

        model.addAttribute("models", carModelService.getAllModels());
        model.addAttribute("brands", carBrandService.getAllBrands());
        model.addAttribute("offer", new OfferDto());

        return "offer";
    }

    @PostMapping("/new")
    public String handleNewOffer(@Valid @ModelAttribute(name = "offer") OfferDto offerDto, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("models", carModelService.getAllModels());
            model.addAttribute("brands", carBrandService.getAllBrands());
            model.addAttribute("offer", offerDto);
            return "offer";
        }
        Offer newOffer = offerMapper.convertDtoToOffer(offerDto);
        offerService.createOffer(newOffer);

        model.addAttribute("newOffer", newOffer);
        return "offer-confirmation";
    }

}
