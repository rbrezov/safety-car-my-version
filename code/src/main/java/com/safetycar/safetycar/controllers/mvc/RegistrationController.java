package com.safetycar.safetycar.controllers.mvc;

import com.safetycar.safetycar.models.User;
import com.safetycar.safetycar.models.UserDetails;
import com.safetycar.safetycar.models.VerificationToken;
import com.safetycar.safetycar.utils.UserMapper;
import com.safetycar.safetycar.models.dtos.UserRegistrationDTO;
import com.safetycar.safetycar.services.VerificationTokenService;
import com.safetycar.safetycar.services.interfaces.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.sql.Timestamp;

import static com.safetycar.safetycar.services.UserDetailsServiceImpl.ENABLED;

@Controller
@RequestMapping("/register")
public class RegistrationController {

    private final UserDetailsService userDetailsService;
    private final UserDetailsManager userDetailsManager;
    private final UserMapper userMapper;
    private final VerificationTokenService verificationTokenService;

    @Autowired
    public RegistrationController(UserDetailsService userDetailsService, UserDetailsManager userDetailsManager,
                                  UserMapper userMapper, VerificationTokenService verificationTokenService) {
        this.userDetailsService = userDetailsService;
        this.userDetailsManager = userDetailsManager;
        this.userMapper = userMapper;
        this.verificationTokenService = verificationTokenService;
    }

    @GetMapping
    public String showRegisterPage(Model model) {
        model.addAttribute("user", new UserRegistrationDTO());
        return "register";
    }

    @PostMapping
    public String registerUser(@Valid UserRegistrationDTO dto, BindingResult bindingResult, Model model) {

        //TODO more precise on type of error
        if (bindingResult.hasErrors()) {
            model.addAttribute("user", dto);
            model.addAttribute("error", "Username/password/First name/Last name cannot be empty!");
            return "register";
        }

        if (userDetailsManager.userExists(dto.getEmailAddress())) {
            model.addAttribute("user", dto);
            model.addAttribute("error", "User with the same email already exists!");
            return "register";
        }

        if (!dto.getPassword().equals(dto.getPasswordConfirmation())) {
            model.addAttribute("user", dto);
            model.addAttribute("error", "Password confirmation does not match password!");
            return "register";
        }

        User userToCreate = userMapper.convertFromRegistrationToUser(dto);
        UserDetails userDetailsToCreate = userMapper.convertFromRegistrationToUserDetails(dto);
        userDetailsService.createUser(userToCreate, userDetailsToCreate);

        return "registration-confirmation";
    }

    @GetMapping("/activation")
    @Transactional
    public String activation(@RequestParam("token") String token, Model model) {
        //create html page activation
        VerificationToken verificationToken = verificationTokenService.findByToken(token);
        if (verificationToken == null) {
            model.addAttribute("message", "Your verification token is invalid!");
        } else {
            User user = verificationToken.getUser();
            if (!user.isIsActive()) {
                //get the current timestamp
                Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

                //check if token is expired
                if (verificationToken.getExpiryDate().before(currentTimestamp)) {
                    model.addAttribute("message", "Your verification token has expired!");

                } else {
                    // the token is valid - the user can be activated
                    user.setIsActive(ENABLED);
                    user.getUserDetails().setIsActive(ENABLED);
                    userDetailsService.updateUserStatus(user, user);
                    model.addAttribute("message", "Your account has successfully activated!");
                }
            } else {
//                the user account is already activated
                model.addAttribute("message", "Your account is already activated.");
            }
        }

//        add activation to security config
        return "activation";
    }
}
