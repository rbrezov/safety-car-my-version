package com.safetycar.safetycar.controllers.mvc;

import com.safetycar.safetycar.models.*;
import com.safetycar.safetycar.models.dtos.PolicyDto;
import com.safetycar.safetycar.services.interfaces.*;
import com.safetycar.safetycar.utils.FileUploadUtil;
import com.safetycar.safetycar.utils.PolicyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequestMapping("/policies")
public class PolicyController {

    private final PolicyService policyService;
    private final OfferService offerService;
    private final CarModelService carModelService;
    private final UserDetailsService userDetailsService;
    private final PolicyStatusService policyStatusService;
    private final PolicyMapper policyMapper;

    @Autowired
    public PolicyController(PolicyService policyService, OfferService offerService, CarModelService carModelService, UserDetailsService userDetailsService, PolicyStatusService policyStatusService, PolicyMapper policyMapper) {
        this.policyService = policyService;
        this.offerService = offerService;
        this.carModelService = carModelService;
        this.userDetailsService = userDetailsService;
        this.policyStatusService = policyStatusService;
        this.policyMapper = policyMapper;
    }


    @ModelAttribute
    private void offersArtibute(Model model) {
        model.addAttribute("offers", offerService.getAllOffers());
    }

    @GetMapping("/new")
    public String getNewPolicyPage(@SessionAttribute("newOffer") Offer newOffer, Model model) {

        model.addAttribute("models", carModelService.getAllModels());
        offersArtibute(model);
        model.addAttribute("policy", new PolicyDto());
        model.addAttribute("newOffer", newOffer);

        return "policy";
    }

    @PostMapping("/new")
    public String handleNewPolicy(@Valid @ModelAttribute(name = "policy") PolicyDto policyDto,
                                  BindingResult bindingResult, @SessionAttribute("newOffer") Offer newOffer,
                                  Model model, Principal principal) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("models", carModelService.getAllModels());
            model.addAttribute("offers", offerService.getAllOffers());
            model.addAttribute("policy", policyDto);
            return "policy";
        }

        UserDetails owner = userDetailsService.getUserDetailsByEmail(principal.getName());

        Policy newPolicy = policyMapper.convertDtoToPolicy(policyDto);

        newPolicy.setOffer(newOffer);
        policyService.createPolicy(newPolicy, owner);

        model.addAttribute("newOffer", newOffer);
        model.addAttribute("newPolicy", newPolicy);
        model.addAttribute("endDate", policyService.getPolicyById(newPolicy.getId()).getEndDate());
        model.addAttribute("submitDate", policyService.getPolicyById(newPolicy.getId()).getSubmitDate());
        model.addAttribute("convertImage", new FileUploadUtil());
        return "policy-confirmation";
    }


    @GetMapping("/{id}/update")
    public String getUpdatePolicyPage(@PathVariable Integer id, Model model) {

        Policy policy = policyService.getPolicyById(id);

        PolicyDto policyDto = policyMapper.convertPolicyToDto(policy);

        model.addAttribute("models", carModelService.getAllModels());
        model.addAttribute("offers", offerService.getAllOffers());
        model.addAttribute("owner", userDetailsService.getAllUsers());
        model.addAttribute("statuses", policyStatusService.getAllStatuses());
        model.addAttribute("policy", policyDto);
        model.addAttribute("id", id);

        return "policy-update";
    }

    @PostMapping("/{id}/update")
    public String handleUpdatePolicy(@PathVariable Integer id,
                                     @Valid @ModelAttribute(name = "policy") PolicyDto policyDto,
                                     BindingResult bindingResult, Model model, Principal principal) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("models", carModelService.getAllModels());
            model.addAttribute("offers", offerService.getAllOffers());
            model.addAttribute("statuses", policyStatusService.getAllStatuses());
            model.addAttribute("requester", userDetailsService.getAllUsers());
            model.addAttribute("policy", policyDto);
            return "policy-update";
        }

        UserDetails requester = userDetailsService.getUserDetailsByEmail(principal.getName());
        Policy policyToUpdate = policyService.getPolicyById(id);
        policyToUpdate = policyMapper.convertDtoToPolicy(policyDto, policyToUpdate);
        policyToUpdate.setPolicyStatus(policyStatusService.getPolicyStatusById(1));
        policyToUpdate.setEndDate(policyService.calculateEndDate(policyToUpdate));

        policyService.updatePolicy(policyToUpdate, requester);

        return "policy-update-confirmation";
    }


    @PostMapping("/{id}/delete")
    public String getDeletePolicyPage(@PathVariable Integer id, Model model, Principal principal) {

        UserDetails requester = userDetailsService.getUserDetailsByEmail(principal.getName());
        Policy policyToDelete = policyService.getPolicyById(id);
        policyService.deletePolicy(policyToDelete, requester);

        model.addAttribute("requester", userDetailsService.getAllUsers());
        model.addAttribute("policy", policyToDelete);
        model.addAttribute("id", id);

        return "policy-delete-confirmation";
    }

    @PostMapping("/{id}/cancel")
    public String handleCancelPolicy(@PathVariable Integer id, Model model, Principal principal){
        Policy policyToCancel = policyService.getPolicyById(id);
        UserDetails requester = userDetailsService.getUserDetailsByEmail(principal.getName());
        policyService.cancelPolicy(policyToCancel, requester);

        model.addAttribute("requester", userDetailsService.getAllUsers());
        model.addAttribute("policy", policyToCancel);
        model.addAttribute("id", id);

        return "policy-cancel-confirmation";
    }

    @PostMapping("/{id}/approve")
    public String handleApprovePolicy(@PathVariable Integer id, Model model, Principal principal){
        Policy policyToApprove = policyService.getPolicyById(id);
        UserDetails requester = userDetailsService.getUserDetailsByEmail(principal.getName());
        policyService.approvePolicy(policyToApprove, requester);

        model.addAttribute("requester", userDetailsService.getAllUsers());
        model.addAttribute("policy", policyToApprove);
        model.addAttribute("id", id);

        return "policy-approve-confirmation";
    }

    @PostMapping("/{id}/reject")
    public String handleRejectPolicy(@PathVariable Integer id, Model model, Principal principal){
        Policy policyToReject = policyService.getPolicyById(id);
        UserDetails requester = userDetailsService.getUserDetailsByEmail(principal.getName());
        policyService.rejectPolicy(policyToReject, requester);

        model.addAttribute("requester", userDetailsService.getAllUsers());
        model.addAttribute("policy", policyToReject);
        model.addAttribute("id", id);

        return "policy-reject-confirmation";
    }
}





























