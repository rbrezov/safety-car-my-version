package com.safetycar.safetycar.controllers.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

    @GetMapping("/home")
    public String showHomepage(){
        return "index";
    }

    @GetMapping("/contacts")
    public String showContactsPage(){
        return "contacts";
    }
}
