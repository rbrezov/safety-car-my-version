package com.safetycar.safetycar.controllers.mvc;

import com.safetycar.safetycar.models.User;
import com.safetycar.safetycar.models.UserDetails;
import com.safetycar.safetycar.models.dtos.PolicyFilterSortDTO;
import com.safetycar.safetycar.utils.UserMapper;
import com.safetycar.safetycar.models.dtos.UserChangePasswordDTO;
import com.safetycar.safetycar.models.dtos.UserDetailsUpdateDTO;
import com.safetycar.safetycar.services.interfaces.OfferService;
import com.safetycar.safetycar.services.interfaces.PolicyService;
import com.safetycar.safetycar.services.interfaces.PolicyStatusService;
import com.safetycar.safetycar.services.interfaces.UserDetailsService;
import com.safetycar.safetycar.utils.FileUploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.security.Principal;

import static com.safetycar.safetycar.controllers.rest.UserRestController.checkIfPasswordsMatch;

@Controller
@RequestMapping("/users")
public class UserController {

    private final UserDetailsService userDetailsService;
    private final PolicyService policyService;
    private final OfferService offerService;
    private final PolicyStatusService policyStatusService;
    private final UserMapper userMapper;

    @Autowired
    public UserController(UserDetailsService userDetailsService, PolicyService policyService, OfferService offerService, PolicyStatusService policyStatusService, UserMapper userMapper) {
        this.userDetailsService = userDetailsService;
        this.policyService = policyService;
        this.offerService = offerService;
        this.policyStatusService = policyStatusService;
        this.userMapper = userMapper;
    }

    @GetMapping
    public String showAllPoliciesByOwner(Principal principal, Model model) {

        model.addAttribute("policies", policyService.getAllPolicies());
        model.addAttribute("offers", offerService.getAllOffers());
        model.addAttribute("statuses", policyStatusService.getAllStatuses());
        model.addAttribute("convertImage", new FileUploadUtil());

        UserDetails owner = userDetailsService.getUserDetailsByEmail(principal.getName());
        policyService.getAllByOwner(owner.getId());

        model.addAttribute("allPoliciesByOwner", policyService.getAllByOwner(owner.getId()));

        return "user";
    }

    @GetMapping("/password")
    public String getChangePasswordPage(Model model) {
        model.addAttribute("dto", new UserChangePasswordDTO());

        return "change-password";
    }

    @PostMapping("/password")
    public String handleChangePassword(@Valid @ModelAttribute(name = "dto") UserChangePasswordDTO dto,
                                       Principal principal,
                                       Model model,
                                       BindingResult bindingResult) {

        User requester = userDetailsService.getUserByEmail(principal.getName());
        checkIfPasswordsMatch(dto.getNewPassword(), dto.getPasswordConfirmation());
        String oldPassword = dto.getOldPassword();
        String newPassword = dto.getNewPassword();

        userDetailsService.changeUserPassword(oldPassword, newPassword, requester);

        if (bindingResult.hasErrors()) {
            model.addAttribute("dto", new UserChangePasswordDTO());
            model.addAttribute("message", bindingResult.getFieldError());
            return "change-password";
        }
        return "user-update-confirmation";
    }

    @GetMapping("/edit")
    public String getEditInfoPage(Model model, Principal principal) {

        UserDetails userDetails = userDetailsService.getUserDetailsByEmail(principal.getName());
        UserDetailsUpdateDTO dto = userMapper.convertFromUserDetailsToUpdateDTO(userDetails);
        model.addAttribute("dto", dto);
        return "user-update";
    }

    @PostMapping("/edit")
    public String handleEditInfo(@Valid @ModelAttribute(name = "dto") UserDetailsUpdateDTO dto,
                                 Model model,
                                 Principal principal,
                                 BindingResult bindingResult){

        User requester = userDetailsService.getUserByEmail(principal.getName());
        dto.setId(requester.getUserDetails().getId());
        UserDetails userDetails = userMapper.convertFromUpdateToUserDetails(dto);
        userDetailsService.updateUserDetails(userDetails, requester);

        if (bindingResult.hasErrors()) {
            model.addAttribute("dto", dto);
            model.addAttribute("message", bindingResult.getFieldError());
            return "user-update";
        }

        return "user-update-confirmation";
    }

    @PostMapping("/filter-status")
    public String filterByStatus(PolicyFilterSortDTO policyFilterSortDTO, Model model){

        model.addAttribute("policyFilterSortDTO", policyFilterSortDTO);
        model.addAttribute("statusId", policyService.getPolicyById(policyFilterSortDTO.getStatusId()));
        model.addAttribute("statuses", policyStatusService.getAllStatuses());


        return "user";
    }

}
