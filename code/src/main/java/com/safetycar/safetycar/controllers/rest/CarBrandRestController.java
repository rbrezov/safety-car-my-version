package com.safetycar.safetycar.controllers.rest;

import com.safetycar.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.safetycar.models.CarBrand;
import com.safetycar.safetycar.services.interfaces.CarBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/brands")
public class CarBrandRestController {

    private final CarBrandService carBrandService;

    @Autowired
    public CarBrandRestController(CarBrandService carBrandService) {
        this.carBrandService = carBrandService;
    }

    @GetMapping("/all")
    public List<CarBrand> getAllBrands() {
        return carBrandService.getAllBrands();
    }

    @GetMapping("/{id}")
    public CarBrand getBrandById(@PathVariable int id) {
        return carBrandService.getBrandById(id);
    }
}
