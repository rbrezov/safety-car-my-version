package com.safetycar.safetycar.controllers.rest;

import com.safetycar.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.safetycar.models.PolicyStatus;
import com.safetycar.safetycar.services.interfaces.PolicyStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/statuses")
public class PolicyStatusRestController {

    private final PolicyStatusService policyStatusService;

    @Autowired
    public PolicyStatusRestController(PolicyStatusService policyStatusService) {
        this.policyStatusService = policyStatusService;
    }

    @GetMapping("/all")
    public List<PolicyStatus> getAllStatuses() {
        return policyStatusService.getAllStatuses();
    }

    @GetMapping("/{id}")
    public PolicyStatus getStatusById(@PathVariable int id) {
        return policyStatusService.getPolicyStatusById(id);
    }
}
