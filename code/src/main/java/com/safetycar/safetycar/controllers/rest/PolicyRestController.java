package com.safetycar.safetycar.controllers.rest;

import com.safetycar.safetycar.models.Policy;
import com.safetycar.safetycar.models.UserDetails;
import com.safetycar.safetycar.models.dtos.PolicyDto;
import com.safetycar.safetycar.services.interfaces.PolicyService;
import com.safetycar.safetycar.services.interfaces.UserDetailsService;
import com.safetycar.safetycar.utils.PolicyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/policies")
public class PolicyRestController {

    private final PolicyService policyService;
    private final UserDetailsService userDetailsService;
    private final PolicyMapper policyMapper;

    @Autowired
    public PolicyRestController(PolicyService policyService, UserDetailsService userDetailsService, PolicyMapper policyMapper) {
        this.policyService = policyService;
        this.userDetailsService = userDetailsService;
        this.policyMapper = policyMapper;
    }

    @GetMapping("/all")
    public List<Policy> getAllPolicies() {
        return policyService.getAllPolicies();
    }

    @GetMapping("/{id}")
    public Policy getPolicyById(@PathVariable int id) {
        return policyService.getPolicyById(id);
    }

    @PostMapping
    public Policy createPolicy(@Valid @RequestBody PolicyDto policyDto, Principal principal) throws IOException {
        Policy newPolicy = policyMapper.convertDtoToPolicy(policyDto);
        UserDetails policyOwner = userDetailsService.getUserDetailsByEmail(principal.getName());
        policyService.createPolicy(newPolicy, policyOwner);
        return newPolicy;
    }

    @PutMapping("/{id}/update")
    public Policy updatePolicy(@PathVariable Integer id, @Valid @RequestBody PolicyDto policyDto, Principal principal) throws IOException {

        Policy policyToUpdate = policyService.getPolicyById(id);
        policyToUpdate = policyMapper.convertDtoToPolicy(policyDto);
        UserDetails requester = userDetailsService.getUserDetailsByEmail(principal.getName());
        policyService.updatePolicy(policyToUpdate, requester);
        return policyToUpdate;
    }

    @DeleteMapping("/{id}/delete")
    public void deletePolicy(@PathVariable int id, Principal principal) {
        Policy policyToDelete = policyService.getPolicyById(id);
        UserDetails requester = userDetailsService.getUserDetailsByEmail(principal.getName());
        policyService.deletePolicy(policyToDelete, requester);
    }

    @PutMapping("/{id}/cancel")
    public void cancelPolicy(@PathVariable Integer id, Principal principal){
        Policy policyToCancel = policyService.getPolicyById(id);
        UserDetails requester = userDetailsService.getUserDetailsByEmail(principal.getName());
        policyService.cancelPolicy(policyToCancel, requester);
    }

    @PutMapping("/{id}/approve")
    public void approvePolicy(@PathVariable Integer id, Principal principal){
        Policy policyToApprove = policyService.getPolicyById(id);
        UserDetails requester = userDetailsService.getUserDetailsByEmail(principal.getName());
        policyService.approvePolicy(policyToApprove, requester);
    }

    @PutMapping("/{id}/reject")
    public void rejectPolicy(@PathVariable Integer id, Principal principal){
        Policy policyToReject = policyService.getPolicyById(id);
        UserDetails requester = userDetailsService.getUserDetailsByEmail(principal.getName());
        policyService.rejectPolicy(policyToReject, requester);
    }
}
