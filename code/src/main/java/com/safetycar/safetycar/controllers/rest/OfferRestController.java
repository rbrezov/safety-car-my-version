package com.safetycar.safetycar.controllers.rest;

import com.safetycar.safetycar.models.Offer;
import com.safetycar.safetycar.models.dtos.OfferDto;
import com.safetycar.safetycar.services.interfaces.OfferService;
import com.safetycar.safetycar.utils.OfferMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/offers")
public class OfferRestController {

    private final OfferService offerService;
    private final OfferMapper offerMapper;

    @Autowired
    public OfferRestController(OfferService offerService, OfferMapper offerMapper) {
        this.offerService = offerService;
        this.offerMapper = offerMapper;
    }

    @GetMapping("/all")
    public List<Offer> getAllOffers() {
        return offerService.getAllOffers();
    }

    @GetMapping("/{id}")
    public Offer getOfferById(@PathVariable int id) {
        return offerService.getOfferById(id);
    }

    @PostMapping
    public Offer createOffer(@Valid @RequestBody OfferDto offerDto) {
        Offer newOffer = offerMapper.convertDtoToOffer(offerDto);
        offerService.createOffer(newOffer);
        return newOffer;
    }

    @DeleteMapping("/{id}")
    public void deleteOffer(@PathVariable int id) {
        offerService.deleteOffer(getOfferById(id));
    }
}
