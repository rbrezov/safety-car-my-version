package com.safetycar.safetycar.controllers.rest;

import com.safetycar.safetycar.exceptions.DuplicateEntityException;
import com.safetycar.safetycar.models.Policy;
import com.safetycar.safetycar.models.User;
import com.safetycar.safetycar.models.UserDetails;
import com.safetycar.safetycar.utils.UserMapper;
import com.safetycar.safetycar.models.dtos.UserChangePasswordDTO;
import com.safetycar.safetycar.models.dtos.UserDetailsUpdateDTO;
import com.safetycar.safetycar.models.dtos.UserRegistrationDTO;
import com.safetycar.safetycar.services.interfaces.PolicyService;
import com.safetycar.safetycar.services.interfaces.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class  UserRestController {

    private final UserDetailsService userDetailsService;
    private final PolicyService policyService;
    private final UserMapper userMapper;

    @Autowired
    public UserRestController(UserDetailsService userDetailsService, PolicyService policyService, UserMapper userMapper) {
        this.userDetailsService = userDetailsService;
        this.policyService = policyService;
        this.userMapper = userMapper;
    }

    @GetMapping("/all")
    public List<UserDetails> getAllUsers() {
        return userDetailsService.getAllUsers();
    }

    @GetMapping("{id}")
    public UserDetails getUserDetailsById(@PathVariable int id) {
        return userDetailsService.getUserDetailsById(id);
    }

    @GetMapping("{id}/policies")
    public List<Policy> getPoliciesByOwner(@PathVariable int id){
        return policyService.getAllByOwner(id);
    }

    @PostMapping("/create")
    public UserDetails createUser(@Valid @RequestBody UserRegistrationDTO dto) {
        checkIfPasswordsMatch(dto.getPassword(), dto.getPasswordConfirmation());
        User user = userMapper.convertFromRegistrationToUser(dto);
        UserDetails userDetails = userMapper.convertFromRegistrationToUserDetails(dto);
        userDetailsService.createUser(user, userDetails);
        return userMapper.convertFromRegistrationToUserDetails(dto);
    }

    @PutMapping("/changePass")
    public boolean changeUserPassword(@Valid @RequestBody UserChangePasswordDTO dto, Principal principal) {
        checkIfPasswordsMatch(dto.getNewPassword(), dto.getPasswordConfirmation());
        String oldPassword = dto.getOldPassword();
        String newPassword = dto.getNewPassword();
        User requester = userDetailsService.getUserByEmail(principal.getName());
        userDetailsService.changeUserPassword(oldPassword, newPassword, requester);
        return true;
    }

    @PutMapping("/editInfo")
    public boolean updateUserDetails(@Valid @RequestBody UserDetailsUpdateDTO dto, Principal principal) {
        User requester = userDetailsService.getUserByEmail(principal.getName());
        UserDetails userDetails = userMapper.convertFromUpdateToUserDetails(dto);
        userDetailsService.updateUserDetails(userDetails, requester);
        return true;
    }

    @DeleteMapping({"{id}"})
    public boolean deleteUser(@PathVariable int id, Principal principal) {
        UserDetails surrogate = userDetailsService.getUserDetailsById(id);
        User userToDelete = userDetailsService.getUserByEmail(surrogate.getEmailAddress());
        User requester = userDetailsService.getUserByEmail(principal.getName());
        userDetailsService.deleteUser(userToDelete, requester);
        return true;
    }

    public static void checkIfPasswordsMatch(String pass, String passConfirm) {
        if (!pass.equals(passConfirm)) {
            throw new DuplicateEntityException("Password does not match password confirmation");
        }
    }
}
