package com.safetycar.safetycar.controllers.mvc;

import com.safetycar.safetycar.services.interfaces.*;
import com.safetycar.safetycar.utils.FileUploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@PreAuthorize("hasRole('ROLE_AGENT')")
@RequestMapping("/agents")
public class AgentController {

    private final UserDetailsService userDetailsService;
    private final PolicyService policyService;
    private final OfferService offerService;
    private final PolicyStatusService policyStatusService;
    private final CarModelService carModelService;

    @Autowired
    public AgentController(UserDetailsService userDetailsService, PolicyService policyService, OfferService offerService, PolicyStatusService policyStatusService, CarModelService carModelService) {
        this.userDetailsService = userDetailsService;
        this.policyService = policyService;
        this.offerService = offerService;
        this.policyStatusService = policyStatusService;
        this.carModelService = carModelService;
    }

    @GetMapping
    public String showAllPolicies(Model model){
        model.addAttribute("models", carModelService.getAllModels());
        model.addAttribute("offers", offerService.getAllOffers());
        model.addAttribute("statuses", policyStatusService.getAllStatuses());
        model.addAttribute("owners", userDetailsService.getAllUsers());
        model.addAttribute("convertImage", new FileUploadUtil());

        model.addAttribute("policies", policyService.getAllPolicies());
        return "agent";
    }
}
