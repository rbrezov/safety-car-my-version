package com.safetycar.safetycar.repositories.interfaces;

import com.safetycar.safetycar.models.User;
import com.safetycar.safetycar.models.VerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VerificationTokenRepository extends JpaRepository<VerificationToken, Long> {
    VerificationToken findByToken(String token);

    VerificationToken findByUser(User user);
}
