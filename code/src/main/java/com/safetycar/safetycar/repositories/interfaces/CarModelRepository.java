package com.safetycar.safetycar.repositories.interfaces;

import com.safetycar.safetycar.models.CarModel;

import java.util.List;

public interface CarModelRepository {

    List<CarModel> getAllModels();

    CarModel getModelById(int id);

    List<CarModel> getModelsByBrand(int id);
}
