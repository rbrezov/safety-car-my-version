package com.safetycar.safetycar.repositories.interfaces;

import com.safetycar.safetycar.models.PolicyStatus;

import java.util.List;

public interface PolicyStatusRepository {

    List<PolicyStatus> getAllStatuses();

    PolicyStatus getPolicyStatusById(int id);

    PolicyStatus getPolicyStatusByName(String name);
}
