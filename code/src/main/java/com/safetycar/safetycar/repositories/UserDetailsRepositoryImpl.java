package com.safetycar.safetycar.repositories;

import com.safetycar.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.safetycar.models.User;
import com.safetycar.safetycar.models.UserDetails;
import com.safetycar.safetycar.repositories.interfaces.UserDetailsRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDetailsRepositoryImpl implements UserDetailsRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserDetailsRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<UserDetails> getAllUsers() {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> query = session.createQuery("from UserDetails", UserDetails.class);
            return query.list();
        }
    }

    @Override
    public UserDetails getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            UserDetails user = session.get(UserDetails.class, id);
            if (user == null) {
                throw new EntityNotFoundException(String.format("User with id %d does not exists!", id));
            }
            return user;
        }
    }

    @Override
    public User getUserByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where emailAddress = :emailAddress", User.class);
            query.setParameter("emailAddress", email);
            List<User> user = query.list();

            if (user.isEmpty()) {
                throw new EntityNotFoundException(String.format("User with email %s does not exists!", email));
            }
            return user.get(0);
        }
    }

    @Override
    public UserDetails getUserDetailsByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> query =
                    session.createQuery("from UserDetails where emailAddress = :emailAddress", UserDetails.class);
            query.setParameter("emailAddress", email);
            List<UserDetails> user = query.list();

            if (user.isEmpty()) {
                throw new EntityNotFoundException(String.format("User with email %s does not exists!", email));
            }
            return user.get(0);
        }
    }

    @Override
    public void createUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.save(user);
        }
    }

    @Override
    public void createUserDetails(UserDetails userDetails) {
        try (Session session = sessionFactory.openSession()) {
            session.save(userDetails);
        }
    }

    @Override
    public void updateUserStatus(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.merge(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void updateUserDetails(UserDetails userDetails) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(userDetails);
            session.getTransaction().commit();
        }
    }

}
