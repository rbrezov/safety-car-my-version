package com.safetycar.safetycar.repositories.interfaces;

import com.safetycar.safetycar.models.Offer;

import java.util.List;

public interface OfferRepository {

    List<Offer> getAllOffers();

    Offer getOfferById(int id);

    void createRequestNumber(Offer offer);

    int getCarAge(Offer offer);

    double getBaseAmount(Offer offer);

    double calculateTotalPremium(Offer offer);

    void createOffer(Offer offer);

    void deleteOffer(Offer offer);
}
