package com.safetycar.safetycar.repositories;

import com.safetycar.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.safetycar.models.Offer;
import com.safetycar.safetycar.repositories.interfaces.OfferRepository;
import org.decimal4j.util.DoubleRounder;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;

@Repository
public class OfferRepositoryImpl implements OfferRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public OfferRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Offer> getAllOffers() {
        try (Session session = sessionFactory.openSession()) {
            Query<Offer> query = session.createQuery("from Offer ", Offer.class);
            return query.list();
        }
    }

    @Override
    public Offer getOfferById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Offer offer = session.get(Offer.class, id);
            if (offer == null) {
                throw new EntityNotFoundException(String.format("Offer with ID %d not found.", id));
            }
            return offer;
        }
    }

    @Override
    public void createRequestNumber(Offer offer) {
        try (Session session = sessionFactory.openSession()) {
            List<Offer> allOffers = getAllOffers();
            if (allOffers.isEmpty()){
                offer.setRequestNumber(100000);
            } else {
                Query<Integer> query = session.createNativeQuery("SELECT request_number from offers order by request_number desc limit 1");
                int nextRequestNumber = query.getSingleResult();
                offer.setRequestNumber(nextRequestNumber + 1);
            }
        }
    }

    @Override
    public double calculateTotalPremium(Offer offer) {
        double totalPremium;
        double baseAmount;
        double netPremium;
        double accidentCoeff = 0.2;
        double driverAgeCoeff = 0.05;
        double tax = 0.1;
        int trustedDrivingAge = 25;

        baseAmount = getBaseAmount(offer);

        if (offer.isPreviousAccidents()) {
            accidentCoeff = baseAmount * accidentCoeff;
        } else accidentCoeff = 0;
        if (offer.getDriverAge() < trustedDrivingAge) {
            driverAgeCoeff = baseAmount * driverAgeCoeff;
        } else driverAgeCoeff = 0;

        netPremium = baseAmount + accidentCoeff + driverAgeCoeff;
        totalPremium = DoubleRounder.round(netPremium + (netPremium * tax), 2);
        offer.setTotalPremium(totalPremium);
        return totalPremium;
    }

    @Override
    public double getBaseAmount(Offer offer) {
        try (Session session = sessionFactory.openSession()) {
            Query<Double> query = session.createNativeQuery("SELECT mr.base_amount FROM safetycar.multicriteria_ranges mr WHERE ?1 between mr.cc_min AND mr.cc_max AND ?2 between mr.car_age_min AND mr.car_age_max");
            query.setParameter(1, offer.getCubicCapacity());
            query.setParameter(2, getCarAge(offer));
            return query.getResultList().get(0);
        }
    }

    @Override
    public int getCarAge(Offer offer) {
        LocalDate registration = offer.getRegistrationDate();
        LocalDate today = LocalDate.now();
        Period passedTime = Period.between(registration, today);
        return passedTime.getYears();
    }


    @Override
    public void createOffer(Offer offer) {
        try (Session session = sessionFactory.openSession()) {
            session.save(offer);
        }
    }

    @Override
    public void deleteOffer(Offer offer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.remove(offer);
            session.getTransaction().commit();
        }
    }
}
