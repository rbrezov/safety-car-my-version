package com.safetycar.safetycar.repositories;

import com.safetycar.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.safetycar.models.Policy;
import com.safetycar.safetycar.repositories.interfaces.PolicyRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public class PolicyRepositoryImpl implements PolicyRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public PolicyRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Policy> getAllPolicies() {
        try (Session session = sessionFactory.openSession()) {
            Query<Policy> query = session.createQuery("from Policy", Policy.class);
            return query.list();
        }
    }

    @Override
    public List<Policy> getAllByOwner(int ownerId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Policy> query = session.createQuery("from Policy where owner.id = :ownerId", Policy.class);
            query.setParameter("ownerId", ownerId);
            return query.list();
        }
    }

    @Override
    public List<Policy> filterByStatus(int ownerId, int statusId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Policy> query = session.createQuery("from Policy where owner.id = :ownerId and policyStatus.id = :statusId", Policy.class);
            query.setParameter("ownerId", ownerId);
            query.setParameter("statusId", statusId);
            return query.list();
        }
    }

    @Override
    public Policy getPolicyById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Policy policy = session.get(Policy.class, id);
            if (policy == null) {
                throw new EntityNotFoundException(String.format("Policy with ID %d not found.", id));
            }
            return policy;
        }
    }

    @Override
    public LocalDate calculateEndDate(Policy policy) {
        LocalDate startDate = policy.getStartDate();
        LocalDate endDate = startDate.plusYears(1);
        policy.setEndDate(endDate);
        return endDate;
    }

    @Override
    public void calculateSubmitDate(Policy policy) {
        LocalDateTime submitDate = LocalDateTime.now();
        policy.setSubmitDate(submitDate);
    }

    @Override
    public void createPolicy(Policy policy) {
        try (Session session = sessionFactory.openSession()) {
            session.save(policy);
        }
    }

    @Override
    public void updatePolicy(Policy policy) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(policy);
            session.getTransaction().commit();
        }
    }

    @Override
    public void deletePolicy(Policy policy) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.remove(policy);
            session.getTransaction().commit();
        }
    }
}
