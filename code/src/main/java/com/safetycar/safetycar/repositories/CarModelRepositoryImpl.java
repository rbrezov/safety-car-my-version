package com.safetycar.safetycar.repositories;

import com.safetycar.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.safetycar.models.CarModel;
import com.safetycar.safetycar.repositories.interfaces.CarModelRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CarModelRepositoryImpl implements CarModelRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CarModelRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<CarModel> getAllModels() {
        try (Session session = sessionFactory.openSession()) {
            Query<CarModel> query = session.createQuery("from CarModel", CarModel.class);
            return query.list();
        }
    }

    @Override
    public CarModel getModelById(int id) {
        try (Session session = sessionFactory.openSession()) {
            CarModel carModel = session.get(CarModel.class, id);
            if (carModel == null) {
                throw new EntityNotFoundException(String.format("Car model with ID %d not found.", id));
            }
            return carModel;
        }
    }

    @Override
    public List<CarModel> getModelsByBrand(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<CarModel> query = session.createQuery("from CarModel where carBrand.id = :id", CarModel.class);
            query.setParameter("id", id);
            return query.list();
        }
    }
}
