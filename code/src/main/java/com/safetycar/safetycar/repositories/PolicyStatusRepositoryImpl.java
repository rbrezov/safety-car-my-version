package com.safetycar.safetycar.repositories;

import com.safetycar.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.safetycar.models.PolicyStatus;
import com.safetycar.safetycar.repositories.interfaces.PolicyStatusRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PolicyStatusRepositoryImpl implements PolicyStatusRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public PolicyStatusRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<PolicyStatus> getAllStatuses() {
        try (Session session = sessionFactory.openSession()) {
            Query<PolicyStatus> query = session.createQuery("from PolicyStatus", PolicyStatus.class);
            return query.list();
        }
    }

    @Override
    public PolicyStatus getPolicyStatusById(int id) {
        try (Session session = sessionFactory.openSession()) {
            PolicyStatus policyStatus = session.get(PolicyStatus.class, id);
            if (policyStatus == null) {
                throw new EntityNotFoundException(String.format("Policy status with ID %d not found.", id));
            }
            return policyStatus;
        }
    }

    @Override
    public PolicyStatus getPolicyStatusByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<PolicyStatus> query = session.createQuery("from PolicyStatus where name like concat('%',:name,'%') " +
                    "order by name desc", PolicyStatus.class);
            query.setParameter("name", name);
            return query.getSingleResult();
        }
    }
}
