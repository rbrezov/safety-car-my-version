package com.safetycar.safetycar.repositories.interfaces;

import com.safetycar.safetycar.models.Policy;

import java.time.LocalDate;
import java.util.List;

public interface PolicyRepository {

    List<Policy> getAllPolicies();

    List<Policy> getAllByOwner(int ownerId);

    List<Policy> filterByStatus(int ownerId, int statusId);

    Policy getPolicyById(int id);

    LocalDate calculateEndDate(Policy policy);

    void calculateSubmitDate(Policy policy);

    void createPolicy(Policy policy);

    void updatePolicy(Policy policy);

    void deletePolicy(Policy policy);
}
