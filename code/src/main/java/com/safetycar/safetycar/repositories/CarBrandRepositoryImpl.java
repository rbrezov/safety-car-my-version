package com.safetycar.safetycar.repositories;

import com.safetycar.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.safetycar.models.CarBrand;
import com.safetycar.safetycar.repositories.interfaces.CarBrandRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CarBrandRepositoryImpl implements CarBrandRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CarBrandRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<CarBrand> getAllBrands() {
        try (Session session = sessionFactory.openSession()) {
            Query<CarBrand> query = session.createQuery("from CarBrand", CarBrand.class);
            return query.list();
        }
    }

    @Override
    public CarBrand getBrandById(int id) {
        try (Session session = sessionFactory.openSession()) {
            CarBrand carBrand = session.get(CarBrand.class, id);
            if (carBrand == null) {
                throw new EntityNotFoundException(String.format("Car brand with ID %d not found.", id));
            }
            return carBrand;
        }
    }
}
