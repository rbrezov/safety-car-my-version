package com.safetycar.safetycar.repositories.interfaces;

import com.safetycar.safetycar.models.CarBrand;

import java.util.List;

public interface CarBrandRepository {

    List<CarBrand> getAllBrands();

    CarBrand getBrandById(int id);
}
