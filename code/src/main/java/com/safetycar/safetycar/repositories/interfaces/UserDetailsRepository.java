package com.safetycar.safetycar.repositories.interfaces;

import com.safetycar.safetycar.models.User;
import com.safetycar.safetycar.models.UserDetails;

import java.util.List;

public interface UserDetailsRepository {
    List<UserDetails> getAllUsers();

    UserDetails getById(int id);

    UserDetails getUserDetailsByEmail(String email);

    User getUserByEmail(String email);

    void createUser(User user);

    void createUserDetails(UserDetails userDetails);

    void updateUserStatus(User user);

    void updateUserDetails(UserDetails userDetails);
}
