package com.safetycar.safetycar.services.interfaces;

import com.safetycar.safetycar.models.User;
import com.safetycar.safetycar.models.UserDetails;

import java.util.List;

public interface UserDetailsService {

    List<UserDetails> getAllUsers();

    UserDetails getUserDetailsById(int id);

    UserDetails getUserDetailsByEmail(String email);

    User getUserByEmail(String email);

    void createUser(User user, UserDetails userDetails);

    void createUserDetails(UserDetails userDetails);

    void changeUserPassword(String oldPassword, String newPassword, User requester);

    void updateUserStatus(User user, User requester);

    void updateUserDetails(UserDetails userDetails, User requester);

    void deleteUser(User userToDelete, User requester);
}
