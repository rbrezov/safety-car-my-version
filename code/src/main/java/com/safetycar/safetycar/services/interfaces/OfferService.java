package com.safetycar.safetycar.services.interfaces;

import com.safetycar.safetycar.models.Offer;

import java.util.List;

public interface OfferService {

    List<Offer> getAllOffers();

    Offer getOfferById(int id);

    int getCarAge(Offer offer);

    void calculateTotalPremium(Offer offer);

    void createOffer(Offer offer);

    void deleteOffer(Offer offer);
}
