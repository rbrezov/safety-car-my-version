package com.safetycar.safetycar.services;

import com.safetycar.safetycar.exceptions.ActionAlreadyPerformedException;
import com.safetycar.safetycar.exceptions.UnauthorizedOperationException;
import com.safetycar.safetycar.models.Policy;
import com.safetycar.safetycar.models.User;
import com.safetycar.safetycar.models.UserDetails;
import com.safetycar.safetycar.repositories.interfaces.PolicyRepository;
import com.safetycar.safetycar.services.interfaces.PolicyService;
import com.safetycar.safetycar.services.interfaces.PolicyStatusService;
import com.safetycar.safetycar.services.interfaces.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class PolicyServiceImpl implements PolicyService {

    private final PolicyRepository policyRepository;
    private final UserDetailsService userDetailsService;
    private final PolicyStatusService policyStatusService;

    @Autowired
    public PolicyServiceImpl(PolicyRepository policyRepository, UserDetailsService userDetailsService, PolicyStatusService policyStatusService) {
        this.policyRepository = policyRepository;
        this.userDetailsService = userDetailsService;
        this.policyStatusService = policyStatusService;
    }

    @Override
    public List<Policy> getAllPolicies() {
        return policyRepository.getAllPolicies();
    }

    @Override
    public List<Policy> getAllByOwner(int ownerId) {
        return policyRepository.getAllByOwner(ownerId);
    }

    @Override
    public List<Policy> filterByStatus(int ownerId, int statusId) {
        return policyRepository.filterByStatus(ownerId, statusId);
    }

    @Override
    public Policy getPolicyById(int id) {
        return policyRepository.getPolicyById(id);
    }

    @Override
    public void createPolicy(Policy policy, UserDetails owner) {
        policy.setOwner(owner);
        policy.setPolicyStatus(policyStatusService.getPolicyStatusById(1));
        policyRepository.calculateEndDate(policy);
        policyRepository.calculateSubmitDate(policy);
        policyRepository.createPolicy(policy);
    }

    @Override
    public void updatePolicy(Policy policy, UserDetails requester) {
        validateOwner(policy, requester);
        policyRepository.updatePolicy(policy);
    }

    @Override
    public void deletePolicy(Policy policy, UserDetails requester) {
        validateOwner(policy, requester);
        policyRepository.deletePolicy(policy);
    }

    @Override
    public void approvePolicy(Policy policy, UserDetails requester) {
        User user = userDetailsService.getUserByEmail(requester.getEmailAddress());
        validateAgent(user);
        checkIfCancelled(policy);
        checkIfApproved(policy);
        policy.setPolicyStatus(policyStatusService.getPolicyStatusById(2));
        policyRepository.updatePolicy(policy);
    }

    @Override
    public void rejectPolicy(Policy policy, UserDetails requester) {
        User user = userDetailsService.getUserByEmail(requester.getEmailAddress());
        validateAgent(user);
        checkIfCancelled(policy);
        checkIfRejected(policy);
        policy.setPolicyStatus(policyStatusService.getPolicyStatusById(3));
        policyRepository.updatePolicy(policy);
    }

    @Override
    public void cancelPolicy(Policy policy, UserDetails requester) {
        validateOwner(policy, requester);
        checkIfCancelled(policy);
        policy.setPolicyStatus(policyStatusService.getPolicyStatusById(5));
        policyRepository.updatePolicy(policy);
    }

    @Override
    public LocalDate calculateEndDate(Policy policy) {
        return policyRepository.calculateEndDate(policy);
    }

    private void validateOwner(Policy policy, UserDetails requester) {
        if (!policy.getOwner().getEmailAddress().equalsIgnoreCase(requester.getEmailAddress()))
            throw new UnauthorizedOperationException("You are not authorized to edit this policy!");
    }

    private void validateAgent(User user) {
        if (!user.isAgent()) {
            throw new UnauthorizedOperationException("You are not authorized to edit this policy!");
        }
    }

    private void checkIfCancelled(Policy policy) {
        if (policy.getPolicyStatus().getId() == 5) {
            throw new ActionAlreadyPerformedException("This policy has already been cancelled.");
        }
    }

    private void checkIfApproved(Policy policy) {
        if (policy.getPolicyStatus().getId() == 2) {
            throw new ActionAlreadyPerformedException("This policy has already been approved.");
        }
    }

    private void checkIfRejected(Policy policy) {
        if (policy.getPolicyStatus().getId() == 3) {
            throw new ActionAlreadyPerformedException("This policy has already been rejected.");
        }
    }
}