package com.safetycar.safetycar.services.interfaces;

import com.safetycar.safetycar.models.CarBrand;

import java.util.List;

public interface CarBrandService {

    List<CarBrand> getAllBrands();

    CarBrand getBrandById(int id);
}
