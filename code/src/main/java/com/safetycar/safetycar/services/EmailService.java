package com.safetycar.safetycar.services;

import com.safetycar.safetycar.models.User;
import com.safetycar.safetycar.models.VerificationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.NoSuchMessageException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class EmailService {

    private final VerificationTokenService verificationTokenService;
    private final TemplateEngine templateEngine;
    private final JavaMailSender javaMailSender;

    @Autowired
    public EmailService(VerificationTokenService verificationTokenService, TemplateEngine templateEngine, JavaMailSender javaMailSender) {
        this.verificationTokenService = verificationTokenService;
        this.templateEngine = templateEngine;
        this.javaMailSender = javaMailSender;
    }

    public void sendHtmlMail(User user) throws NoSuchMessageException, MessagingException {
        VerificationToken verificationToken = verificationTokenService.findByUser(user);

        //check if the user has a token
        if (verificationToken != null) {
            String token = verificationToken.getToken();
            Context context = new Context();
            context.setVariable("title", "Verify your email address");
            context.setVariable("link", "http://localhost:8080/register/activation?token=" + token);
            context.setVariable("firstName", user.getUserDetails().getFirstName());
            context.setVariable("lastName", user.getUserDetails().getLastName());

            // create and HTML template and pass the variable to it
            String body = templateEngine.process("verification", context);

            // Send the verification email
            MimeMessage message = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setTo(user.getEmailAddress());
            helper.setSubject("Confirm your registration - Safety Car");
            helper.setText(body, true);
            javaMailSender.send(message);
        }
    }
}
