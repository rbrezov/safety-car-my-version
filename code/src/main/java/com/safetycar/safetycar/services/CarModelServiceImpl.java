package com.safetycar.safetycar.services;

import com.safetycar.safetycar.models.CarModel;
import com.safetycar.safetycar.repositories.interfaces.CarModelRepository;
import com.safetycar.safetycar.services.interfaces.CarModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarModelServiceImpl implements CarModelService {

    private final CarModelRepository carModelRepository;

    @Autowired
    public CarModelServiceImpl(CarModelRepository carModelRepository) {
        this.carModelRepository = carModelRepository;
    }


    @Override
    public List<CarModel> getAllModels() {
        return carModelRepository.getAllModels();
    }

    @Override
    public CarModel getModelById(int id) {
        return carModelRepository.getModelById(id);
    }

    @Override
    public List<CarModel> getModelsByBrand(int id) {
        return carModelRepository.getModelsByBrand(id);
    }
}
