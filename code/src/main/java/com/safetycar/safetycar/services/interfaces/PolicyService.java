package com.safetycar.safetycar.services.interfaces;

import com.safetycar.safetycar.models.Policy;
import com.safetycar.safetycar.models.UserDetails;

import java.time.LocalDate;
import java.util.List;

public interface PolicyService {

    List<Policy> getAllPolicies();

    List<Policy> getAllByOwner(int ownerId);

    List<Policy> filterByStatus(int ownerId, int statusId);

    Policy getPolicyById(int id);

    void createPolicy(Policy policy, UserDetails owner);

    void updatePolicy(Policy policy, UserDetails owner);

    void deletePolicy(Policy policy, UserDetails owner);

    void approvePolicy(Policy policy, UserDetails requester);

    void rejectPolicy(Policy policy, UserDetails requester);

    void cancelPolicy(Policy policy, UserDetails requester);

    LocalDate calculateEndDate(Policy policy);
}
