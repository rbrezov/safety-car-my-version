package com.safetycar.safetycar.services.interfaces;

import com.safetycar.safetycar.models.PolicyStatus;

import java.util.List;

public interface PolicyStatusService {

    List<PolicyStatus> getAllStatuses();

    PolicyStatus getPolicyStatusById(int id);

    PolicyStatus getPolicyStatusByName(String name);
}
