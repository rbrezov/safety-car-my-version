package com.safetycar.safetycar.services;

import com.safetycar.safetycar.models.PolicyStatus;
import com.safetycar.safetycar.repositories.interfaces.PolicyStatusRepository;
import com.safetycar.safetycar.services.interfaces.PolicyStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PolicyStatusServiceImpl implements PolicyStatusService {

    private final PolicyStatusRepository policyStatusRepository;

    @Autowired
    public PolicyStatusServiceImpl(PolicyStatusRepository policyStatusRepository) {
        this.policyStatusRepository = policyStatusRepository;
    }

    @Override
    public List<PolicyStatus> getAllStatuses() {
        return policyStatusRepository.getAllStatuses();
    }

    @Override
    public PolicyStatus getPolicyStatusById(int id) {
        return policyStatusRepository.getPolicyStatusById(id);
    }

    @Override
    public PolicyStatus getPolicyStatusByName(String name) {
        return policyStatusRepository.getPolicyStatusByName(name);
    }
}
