package com.safetycar.safetycar.services;

import com.safetycar.safetycar.models.User;
import com.safetycar.safetycar.models.VerificationToken;
import com.safetycar.safetycar.repositories.interfaces.VerificationTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.Calendar;

@Service
public class VerificationTokenService {

    public static final int EXPIRY_TIME_IN_MINUTES = 24 * 60;
    private final VerificationTokenRepository verificationTokenRepository;

    @Autowired
    public VerificationTokenService(VerificationTokenRepository verificationTokenRepository) {
        this.verificationTokenRepository = verificationTokenRepository;
    }

    @Transactional
    public VerificationToken findByToken(String token) {
        return verificationTokenRepository.findByToken(token);
    }

    @Transactional
    public VerificationToken findByUser(User user) {
        return verificationTokenRepository.findByUser(user);
    }

    @Transactional
    public void save(User user, String token) {
        VerificationToken verificationToken = new VerificationToken(token, user);
        // set expiry date to 24 hours
        verificationToken.setExpiryDate(calculateExpiryDate());
        verificationTokenRepository.save(verificationToken);
    }

    private Timestamp calculateExpiryDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, VerificationTokenService.EXPIRY_TIME_IN_MINUTES);
        return new Timestamp(calendar.getTime().getTime());
    }
}
