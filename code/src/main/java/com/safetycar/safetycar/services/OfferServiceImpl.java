package com.safetycar.safetycar.services;

import com.safetycar.safetycar.models.Offer;
import com.safetycar.safetycar.repositories.interfaces.OfferRepository;
import com.safetycar.safetycar.services.interfaces.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OfferServiceImpl implements OfferService {

    private final OfferRepository offerRepository;

    @Autowired
    public OfferServiceImpl(OfferRepository offerRepository) {
        this.offerRepository = offerRepository;
    }

    @Override
    public List<Offer> getAllOffers() {
        return offerRepository.getAllOffers();
    }

    @Override
    public Offer getOfferById(int id) {
        return offerRepository.getOfferById(id);
    }

    @Override
    public int getCarAge(Offer offer) {
        return offerRepository.getCarAge(offer);
    }

    @Override
    public void calculateTotalPremium(Offer offer) {
        offerRepository.calculateTotalPremium(offer);
    }

    @Override
    public void createOffer(Offer offer) {
        offerRepository.createRequestNumber(offer);
        offerRepository.calculateTotalPremium(offer);
        offerRepository.createOffer(offer);
    }

    @Override
    public void deleteOffer(Offer offer) {
        offerRepository.deleteOffer(offer);
    }
}
