package com.safetycar.safetycar.services;

import com.safetycar.safetycar.exceptions.DuplicateEntityException;
import com.safetycar.safetycar.exceptions.UnauthorizedOperationException;
import com.safetycar.safetycar.models.User;
import com.safetycar.safetycar.models.UserDetails;
import com.safetycar.safetycar.repositories.interfaces.UserDetailsRepository;
import com.safetycar.safetycar.services.interfaces.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    public static final boolean ENABLED = true;
    public static final boolean DISABLED = false;

    private final UserDetailsRepository userDetailsRepository;
    private final UserDetailsManager userDetailsManager;
    private final PasswordEncoder passwordEncoder;
    private final VerificationTokenService verificationTokenService;
    private final EmailService emailService;

    @Autowired
    public UserDetailsServiceImpl(UserDetailsRepository userDetailsRepository, UserDetailsManager userDetailsManager,
                                  PasswordEncoder passwordEncoder, VerificationTokenService verificationTokenService, EmailService emailService) {
        this.userDetailsRepository = userDetailsRepository;
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.verificationTokenService = verificationTokenService;
        this.emailService = emailService;
    }

    @Override
    public List<UserDetails> getAllUsers() {
        return userDetailsRepository.getAllUsers();
    }

    @Override
    public UserDetails getUserDetailsById(int id) {
        return userDetailsRepository.getById(id);
    }

    @Override
    public UserDetails getUserDetailsByEmail(String email) {
        return userDetailsRepository.getUserDetailsByEmail(email);
    }

    @Override
    public User getUserByEmail(String email) {
        return userDetailsRepository.getUserByEmail(email);
    }

    @Override
    @Transactional
    public void createUser(User userToCreate, UserDetails userDetailsToCreate) {
        if (userDetailsManager.userExists(userToCreate.getEmailAddress())) {
            throw new DuplicateEntityException(
                    String.format("Email %s is already in use!", userToCreate.getEmailAddress()));
        }

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        userToCreate.getEmailAddress(), passwordEncoder.encode(userToCreate.getPassword()), authorities);
        userDetailsManager.createUser(newUser);

        userToCreate = getUserByEmail(newUser.getUsername());
        createUserDetails(userDetailsToCreate);
        userToCreate.setUserDetails(userDetailsToCreate);
        userToCreate.setIsActive(DISABLED);
        userDetailsToCreate.setIsActive(DISABLED);

        userDetailsRepository.updateUserStatus(userToCreate);
//        userDetailsRepository.updateUserDetails(userDetailsToCreate);

        try {
            String token = UUID.randomUUID().toString();
            verificationTokenService.save(userToCreate, token);

            // send verification email
            emailService.sendHtmlMail(userToCreate);

        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }

    @Override
    public void createUserDetails(UserDetails userDetails) {
        userDetailsRepository.createUserDetails(userDetails);
    }

    @Override
    public void changeUserPassword(String oldPassword, String newPassword, User requester) {
        if (!passwordEncoder.matches(oldPassword, requester.getPassword())) {
            throw new UnauthorizedOperationException("Old password is incorrect.");
        }
        userDetailsManager.changePassword(requester.getPassword(), passwordEncoder.encode(newPassword));
    }

    @Override
    public void updateUserStatus(User user, User requester) {
        checkUserAuthority(user, requester);

        userDetailsRepository.updateUserStatus(user);
    }

    @Override
    public void updateUserDetails(UserDetails userDetails, User requester) {
        UserDetails emailSurrogate = getUserDetailsById(userDetails.getId());
        checkUserAuthority(getUserByEmail(emailSurrogate.getEmailAddress()), requester);

        userDetails.setEmailAddress(emailSurrogate.getEmailAddress());
        userDetailsRepository.updateUserDetails(userDetails);
    }

    @Override
    public void deleteUser(User userToDelete, User requester) {
        checkUserAuthority(userToDelete, requester);

        UserDetails userDetailsToDelete = getUserDetailsByEmail(userToDelete.getEmailAddress());
        userToDelete.setIsActive(DISABLED);
        userDetailsToDelete.setIsActive(DISABLED);

        userDetailsRepository.updateUserStatus(userToDelete);
        userDetailsRepository.updateUserDetails(userDetailsToDelete);
    }

    private void checkUserAuthority(User userToAction, User requester) {
        if (!userToAction.getEmailAddress().equalsIgnoreCase(requester.getEmailAddress()) && !requester.isAdmin()) {
            throw new UnauthorizedOperationException(
                    String.format("You are not authorized to edit user %s", userToAction.getEmailAddress()));
        }
    }

}
