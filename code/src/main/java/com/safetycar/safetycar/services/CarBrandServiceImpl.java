package com.safetycar.safetycar.services;

import com.safetycar.safetycar.models.CarBrand;
import com.safetycar.safetycar.repositories.interfaces.CarBrandRepository;
import com.safetycar.safetycar.services.interfaces.CarBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarBrandServiceImpl implements CarBrandService {

    private final CarBrandRepository carBrandRepository;

    @Autowired
    public CarBrandServiceImpl(CarBrandRepository carBrandRepository) {
        this.carBrandRepository = carBrandRepository;
    }

    @Override
    public List<CarBrand> getAllBrands() {
        return carBrandRepository.getAllBrands();
    }

    @Override
    public CarBrand getBrandById(int id) {
        return carBrandRepository.getBrandById(id);
    }
}
