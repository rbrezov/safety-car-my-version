package com.safetycar.safetycar.services.interfaces;

import com.safetycar.safetycar.models.CarModel;

import java.util.List;

public interface CarModelService {

    List<CarModel> getAllModels();

    CarModel getModelById(int id);

    List<CarModel> getModelsByBrand(int id);
}
