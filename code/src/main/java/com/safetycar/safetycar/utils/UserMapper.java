package com.safetycar.safetycar.utils;

import com.safetycar.safetycar.models.*;
import com.safetycar.safetycar.models.dtos.UserDetailsUpdateDTO;
import com.safetycar.safetycar.models.dtos.UserRegistrationDTO;
import com.safetycar.safetycar.services.interfaces.CarBrandService;
import com.safetycar.safetycar.services.interfaces.CarModelService;
import com.safetycar.safetycar.services.interfaces.OfferService;
import com.safetycar.safetycar.services.interfaces.PolicyStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    private final CarModelService carModelService;
    private final OfferService offerService;
    private final PolicyStatusService policyStatusService;
    private final CarBrandService carBrandService;

    @Autowired
    public UserMapper(CarModelService carModelService, OfferService offerService, PolicyStatusService policyStatusService, CarBrandService carBrandService) {
        this.carModelService = carModelService;
        this.offerService = offerService;
        this.policyStatusService = policyStatusService;
        this.carBrandService = carBrandService;
    }

    public UserDetails convertFromRegistrationToUserDetails(UserRegistrationDTO dto) {
        UserDetails userDetails = new UserDetails();

        if (dto.getPhoneNumber() == null) {
            dto.setPhoneNumber("");
        }
        if (dto.getAddress() == null) {
            dto.setAddress("");
        }
        userDetails.setFirstName(dto.getFirstName());
        userDetails.setLastName(dto.getLastName());
        userDetails.setPhoneNumber(dto.getPhoneNumber());
        userDetails.setAddress(dto.getAddress());
        userDetails.setEmailAddress(dto.getEmailAddress());

        return userDetails;
    }

    public User convertFromRegistrationToUser(UserRegistrationDTO dto) {
        User user = new User();
        user.setEmailAddress(dto.getEmailAddress());
        user.setPassword(dto.getPassword());

        return user;
    }

    public UserDetails convertFromUpdateToUserDetails(UserDetailsUpdateDTO dto) {

        UserDetails userDetails = new UserDetails();
        if (dto.getPhoneNumber() == null) {
            dto.setPhoneNumber("");
        }
        if (dto.getAddress() == null) {
            dto.setAddress("");
        }

        userDetails.setFirstName(dto.getFirstName());
        userDetails.setLastName(dto.getLastName());
        userDetails.setPhoneNumber(dto.getPhoneNumber());
        userDetails.setAddress(dto.getAddress());
        userDetails.setId(dto.getId());

        return userDetails;
    }

    public UserDetailsUpdateDTO convertFromUserDetailsToUpdateDTO(UserDetails userDetails) {

        UserDetailsUpdateDTO dto = new UserDetailsUpdateDTO();
        if (userDetails.getPhoneNumber() == null) {
            userDetails.setPhoneNumber("");
        }
        if (userDetails.getAddress() == null) {
            userDetails.setAddress("");
        }

        dto.setFirstName(userDetails.getFirstName());
        dto.setLastName(userDetails.getLastName());
        dto.setPhoneNumber(userDetails.getPhoneNumber());
        dto.setAddress(userDetails.getAddress());
        dto.setId(userDetails.getId());

        return dto;
    }
}













