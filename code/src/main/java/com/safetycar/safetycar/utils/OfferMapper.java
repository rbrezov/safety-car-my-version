package com.safetycar.safetycar.utils;

import com.safetycar.safetycar.models.CarBrand;
import com.safetycar.safetycar.models.CarModel;
import com.safetycar.safetycar.models.Offer;
import com.safetycar.safetycar.models.dtos.OfferDto;
import com.safetycar.safetycar.services.interfaces.CarBrandService;
import com.safetycar.safetycar.services.interfaces.CarModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OfferMapper {

    private final CarModelService carModelService;
    private final CarBrandService carBrandService;

    @Autowired
    public OfferMapper(CarModelService carModelService, CarBrandService carBrandService) {
        this.carModelService = carModelService;
        this.carBrandService = carBrandService;
    }

    public Offer convertDtoToOffer(OfferDto offerDto) {

        Offer offer = new Offer();

        offer.setCubicCapacity(offerDto.getCubicCapacity());
        CarBrand carBrand = carBrandService.getBrandById(offerDto.getCarBrandId());
        List<CarModel> carModels = carModelService.getModelsByBrand(offerDto.getCarBrandId());
        CarModel carModel = carModels.stream().filter(model -> model.getId() == offerDto.getCarModelId()).findFirst().orElseThrow();
        offer.setCarModel(carModel);
        offer.setCarModel(carModelService.getModelById(offerDto.getCarModelId()));
        offer.setRegistrationDate(offerDto.getRegistrationDate());
        offer.setDriverAge(offerDto.getDriverAge());
        offer.setPreviousAccidents(offerDto.isPreviousAccidents());

        return offer;
    }
}
