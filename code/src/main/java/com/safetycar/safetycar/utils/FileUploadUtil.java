package com.safetycar.safetycar.utils;

import java.util.Base64;

public class FileUploadUtil {

    public String getConvertedImage(byte[] byteImage) {
        return Base64.getMimeEncoder().encodeToString(byteImage);
    }
}
