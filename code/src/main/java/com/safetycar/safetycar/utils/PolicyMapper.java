package com.safetycar.safetycar.utils;

import com.safetycar.safetycar.exceptions.EntityNotFoundException;
import com.safetycar.safetycar.models.Policy;
import com.safetycar.safetycar.models.dtos.PolicyDto;
import com.safetycar.safetycar.services.interfaces.CarBrandService;
import com.safetycar.safetycar.services.interfaces.CarModelService;
import com.safetycar.safetycar.services.interfaces.OfferService;
import com.safetycar.safetycar.services.interfaces.PolicyStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Component
public class PolicyMapper {

    private final CarModelService carModelService;
    private final OfferService offerService;
    private final PolicyStatusService policyStatusService;
    private final CarBrandService carBrandService;

    @Autowired
    public PolicyMapper(CarModelService carModelService, OfferService offerService, PolicyStatusService policyStatusService, CarBrandService carBrandService) {
        this.carModelService = carModelService;
        this.offerService = offerService;
        this.policyStatusService = policyStatusService;
        this.carBrandService = carBrandService;
    }

    public Policy convertDtoToPolicy(PolicyDto policyDto){

        try {
            Policy policy = new Policy();

            policy.setStartDate(policyDto.getStartDate());
            policy.setFirstName(policyDto.getFirstName());
            policy.setLastName(policyDto.getLastName());
            policy.setPhoneNumber(policyDto.getPhoneNumber());
            policy.setEmailAddress(policyDto.getEmailAddress());
            policy.setPostalAddress(policyDto.getPostalAddress());
            MultipartFile multipartFile = policyDto.getRegistrationCertificate();
            policy.setRegistrationCertificate(multipartFile.getBytes());
            return policy;
        } catch (IOException e){
            throw new EntityNotFoundException("Not found");
        }
    }

    public Policy convertDtoToPolicy(PolicyDto policyDto, Policy policy) {

        try {
            policy.setStartDate(policyDto.getStartDate());
            policy.setFirstName(policyDto.getFirstName());
            policy.setLastName(policyDto.getLastName());
            policy.setPhoneNumber(policyDto.getPhoneNumber());
            policy.setEmailAddress(policyDto.getEmailAddress());
            policy.setPostalAddress(policyDto.getPostalAddress());
            MultipartFile multipartFile = policyDto.getRegistrationCertificate();
            policy.setRegistrationCertificate(multipartFile.getBytes());
            return policy;
        } catch (IOException e){
            throw new EntityNotFoundException("Not found");
        }
    }


    public PolicyDto convertPolicyToDto(Policy policy) {

        PolicyDto policyDto = new PolicyDto();

        policyDto.setStartDate(policy.getStartDate());
        policyDto.setFirstName(policy.getFirstName());
        policyDto.setLastName(policy.getLastName());
        policyDto.setEmailAddress(policy.getEmailAddress());
        policyDto.setPhoneNumber(policy.getPhoneNumber());
        policyDto.setPostalAddress(policy.getPostalAddress());

//        MultipartFile multipartFile = policyDto.getRegistrationCertificate();
//        policy.setRegistrationCertificate(multipartFile.getBytes());
        return policyDto;
//        } catch (IOException e) {
//            throw new EntityNotFoundException("Not found");
//        }
    }
}
